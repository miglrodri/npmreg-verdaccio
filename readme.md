### commands

`npm adduser --registry http://localhost:4873`

`npm login --registry=http://localhost:4873 --scope=@mjesus --always-auth=true`

`npm publish`

### edit config

`C:\Users\mfjesus\AppData\Roaming\verdaccio\config.yaml`

~~~~
packages:
  '@mjesus/*':
    # scoped packages
    access: $all
    publish: $authenticated
    proxy: npmjs

  # '**':
  #   # allow all users (including non-authenticated users) to read and
  #   # publish all packages
  #   #
  #   # you can specify usernames/groupnames (depending on your auth plugin)
  #   # and three keywords: "$all", "$anonymous", "$authenticated"
  #   access: $all

  #   # allow all known users to publish packages
  #   # (anyone can register by default, remember?)
  #   publish: $authenticated

  #   # if package is not available locally, proxy requests to 'npmjs' registry
  #   proxy: npmjs
  ~~~~
